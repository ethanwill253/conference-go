import requests
import json
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(city, state):
    headers = {'Authorization': PEXELS_API_KEY}
    photo_params = {
        "per_page": 1,
        "query": f'{city} {state}',
    }
    url = 'https://api.pexels.com/v1/search'
    res = requests.get(url, params=photo_params, headers=headers)
    content = json.loads(res.content)
    try:
        return content["photos"][0]["src"]["original"]
    except:
        return None


def get_weather_data(city, state):
    # Use the Open Weather A
    # PI
    geo_url = 'http://api.openweathermap.org/geo/1.0/direct'
    geo_params = {
        "q": f'{city}, {state}, US',
        "appid": "304d3c448e7ba8b52f610c8714787b0a"
    }
    geo_res = requests.get(geo_url, params=geo_params)
    content = json.loads(geo_res.content)
    try:
        lat = content[0]['lat']
        lon = content[0]['lon']

        weather_url = 'https://api.openweathermap.org/data/2.5/weather'
        weather_params = {
            "lat": lat,
            "lon": lon,
            "appid": OPEN_WEATHER_API_KEY,
            "units": "imperial"
        }
        weather_res = requests.get(weather_url, weather_params)
        content = json.loads(weather_res.content)
        print(content)
        description = content["weather"][0]["description"]
        temp = content["main"]["temp"]
        return {
                "description": description,
                "temp": f'{temp}F'
            }
    except:
        return None
